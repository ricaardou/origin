<?php
	/**
	 * GIT DEPLOYMENT SCRIPT
	 *
	 * Used for automatically deploying websites via github or bitbucket, more deets here:
	 *
	 *		https://gist.github.com/1809044
	 */

	// The commands
	$commands = array(
		'echo $PWD',
		'whoami',
		'sudo git pull',
		'sudo git status',
		'sudo git submodule sync',
		'sudo git submodule update',
		'sudo git submodule status'
		);

	// Run the commands for output
	$output = '';

	foreach($commands AS $command){
		// Run it
		$tmp = shell_exec($command. ' 2>&1');
		// Output
		$output .= "<span style=\"color: #6BE234;\">\$</span> <span style=\"color: #729FCF;\">{$command}\n</span>";
		$output .= htmlentities(trim($tmp)) . "\n";
	}

	// Make it pretty for manual user access (and why not?)
	?>
	<!DOCTYPE HTML>
	<html lang="en-US">
	<head>
		<meta charset="UTF-8">
		<title>GIT DEPLOYMENT SCRIPT</title>
	</head>
	<body style="background-color: #000000; color: #FFFFFF; font-weight: bold; padding: 0 10px;">
		<?php if(isset($_GET["show"]) and is_numeric($_GET["show"]) and $_GET["show"] == 1){ ?>
		<pre>
			.  ____  .    ____________________________
			|/      \|   |                            |
			[| <span style="color: #FF0000;">&hearts;    &hearts;</span> |]  | Git Deployment Script v0.2 |
			|___==___|  /              &copy; oodavid 2012 |
			|____________________________|

			<?php echo $output; ?>
			
		</pre>
		<?php } ?>
	</body>
	</html>
